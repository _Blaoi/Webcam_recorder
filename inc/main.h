#ifndef __MAIN_H_
#define __MAIN_H_

typedef int bool;
#define true 1
#define false 0

typedef struct{
  char *path;
}camera_device;
/*
  print.c
*/
void print_help_quiet(void);
void print_help_verbose(void);
/*
  main.c
*/
void print_available_devices(int *device_counter,camera_device **devices);
void print_devices(void);
int verify_arguments(int argc,char **argv);
int camera_choice(int *device_counter);
#endif
