# Constants

CC=gcc
CFLAGS=-W -Wall -Wno-unused-parameter -Wno-unused-but-set-parameter -Werror -g -O0
S=src
O=bin
H=inc

# Compilation
all: Webcam_recorder

Webcam_recorder:$(O)/print.o $(O)/main.o
	$(CC) -o $@ $^ $(CFLAGS)

$(O)/main.o:$(S)/main.c
	$(CC) -c $^ -o $@ $(CFLAGS)

$(O)/print.o:$(S)/print.c
	$(CC) -c $^ -o $@ $(CFLAGS)

# Other rules
clean:
	rm -rf $(O)/*.o

mrproper: clean
	rm -rf Webcam_recorder
