#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "../inc/main.h"

/*
  Redirect all the arguments to
  'verify_arguments' and the exec code returned
  depend of this function
*/
int main(int argc,char **argv){
  return verify_arguments(argc,argv);
}

/*
  Verify the command arguments
  argc: The number of words put in the command line
  argv: The words put in the command line
*/
int verify_arguments(int argc, char **argv){
  bool h_flag = false;
  bool H_flag = false;
  bool p_flag = false;
  int c;
  char *flags = "hHp";
  while((c = getopt(argc,argv,flags)) != -1){
    switch(c)
      {
      case 'h':
	h_flag = true;
	break;
      case 'H':
	H_flag = true;
	break;
      case 'p':
	p_flag = true;
	break;
      case '?':
	fprintf(stderr,"Unknown option character `\\x%x'.\n",optopt);
	break;
      default:
	abort();
      }
  }
  if(h_flag){
    print_help_quiet();
  }
  else if(H_flag){
    print_help_verbose();
  }else if(p_flag){
    print_devices();
  }else{ //no options specified
    int *device_counter = NULL;
    int maximum_device = 0;
    device_counter = &maximum_device;
    camera_device **devices = NULL;
    print_available_devices(device_counter,devices);
    int choice = camera_choice(device_counter);
    printf("Your choice is:%d\n",choice);
  }
  return 0;
}

int camera_choice(int *device_counter){
  int choice = 0;
  int result_scan = EOF;
  
  while(result_scan == EOF || result_scan != 1){
    result_scan = scanf("%d",&choice);
  }
  return choice;
}
