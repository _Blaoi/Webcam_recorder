/*
  Contains all the print methods used by the program.
*/

#include "../inc/main.h"
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
/*
  Display the main usages of the program.
*/
void print_help_quiet(void){
  printf("Quiet help\n");
  printf("./Webcam_recorder -p:\t Print the available devices\n");
  printf("./Webcam_recorder -h:\t Quiet help output\n");
  printf("./Webcam_recorder -H:\t Verbose help output\n");
}

/*
  Display all the usages of the program
*/
void print_help_verbose(void){
  printf("Verbose help\n");
  printf("HELP COMMANDS\n");
  printf("./Webcam_recorder -h:\t Quiet help output\n");
  printf("./Webcam_recorder -H:\t Verbose help output\n");
}

/*
  Display all the available camera that the user can use.
  *device_counter is the number of available devices
  **device_name contains all the name of the devices
*/
void print_available_devices(int *device_counter,camera_device **devices){;
  printf("Which camera would you like to use ?\n");
  DIR *dp;
  struct dirent *ep;
  //int dc = 0;
  //device_counter = &dc;
  dp = opendir ("/dev/");
  if (dp){
    while((ep = readdir(dp))){
      if(strstr(ep->d_name,"video")){
	(*device_counter)++;
	printf("%d: /dev/%s\n",(*device_counter),ep->d_name);
      }
    }
    if(!*device_counter){
      printf("No camera available in /dev/video*\n");
    }
    (void)closedir(dp);
  }
  else{
    perror("Tried to list devices from /dev/video*\n"
	   "Could not find or open the /dev/ directory\n"
	   "Errno");
  }
}

void print_devices(void){;
  DIR *dp;
  struct dirent *ep;     
  int dc = 0;
  dp = opendir ("/dev/");
  if (dp != NULL){
    dc = 0;
    while((ep = readdir(dp))){
      if(strstr(ep->d_name,"video") != NULL){
	dc++;
	printf("%d: /dev/%s\n",dc,ep->d_name);
      }
    }
    if(dc == 0){
      printf("No camera available in /dev/video*\n");
    }
    (void)closedir(dp);
  }
  else{
    perror("Tried to list devices from /dev/video*\n"
	   "Could not find or open the /dev/ directory\n"
	   "Errno");
  }
}
